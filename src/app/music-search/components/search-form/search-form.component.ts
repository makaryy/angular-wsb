import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Output() search = new EventEmitter<string>();

  @Input() query = ''

  submit() {
    this.search.emit(this.query)
  }

  constructor() { }

  ngOnInit(): void {
  }


  // ngOnChanges(changes: SimpleChanges): void {
  //   const field = changes['_queryFromParent']
  //   if (field.currentValue !== field.previousValue) {
  //     this.localQuery = field.currentValue
  //   }
  // }
}

import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { concatAll, exhaust, exhaustMap, filter, map, mergeAll, switchAll, switchMap, tap } from 'rxjs/operators';
import { Album, SimpleAlbum } from 'src/app/core/model/Search';
import { MusicSearchService } from 'src/app/core/services/music-search.service';


@Component({
  selector: 'app-album-search-view',
  templateUrl: './album-search-view.component.html',
  styleUrls: ['./album-search-view.component.scss']
})
export class AlbumSearchViewComponent implements OnInit {
  query = ''
  results: Album[] = []
  message = ''

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) { }

  ngOnInit(): void {

    this.route.queryParamMap.pipe(
      // Extract param 'q'
      map(p => p.get('q')),
      // skip empty 'q' values
      filter((q): q is string => q !== null),
      // save q to this.query
      tap(q => this.query = q),
      // for each q - make a request to server
      switchMap(q => this.service.search(q)),
    )
      .subscribe({
        next: (albums: Album[]) => this.results = albums,
        error: (error) => this.message = error.message,
      })
  }

  searchAlbums(query: string) {

    this.router.navigate(['/search', 'albums'], {
      queryParams: { q: query }
    })

  }


}

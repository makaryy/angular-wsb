import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass: MyAwesomeExtraBetterHttpClient
    // }
  ]
})
export class CoreModule {
  constructor(private auth: AuthService) {
    auth.init()
  }
}

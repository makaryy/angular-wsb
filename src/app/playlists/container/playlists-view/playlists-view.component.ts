import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

type ViewModes = 'details' | 'edit';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {
  mode: ViewModes = 'details'

  playlists: Playlist[] = [{
    id: '123',
    name: 'Playlista z View 123',
    public: true,
    description: 'opis 1<3'
  }, {
    id: '234',
    name: 'Playlista 234',
    public: false,
    description: 'opis 2<3'
  }, {
    id: '345',
    name: 'Playlista 345',
    public: true,
    description: 'opis 3<3'
  }]

  selectedPlaylist?: Playlist

  constructor() { }

  selectPlaylist(playlist_id: string) {
    this.selectedPlaylist = this.playlists.find(p => p.id == playlist_id)
  }

  savePlaylist(draft: Playlist) {
    const index = this.playlists.findIndex(p => p.id === draft.id)
    if (index !== -1) {
      // this.playlists.splice(index, 1, draft)
      this.playlists[index] = draft
    }
    // this.selectedPlaylist = draft
    this.selectPlaylist(draft.id)
    this.mode = 'details'
  }

  editMode() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'details'
  }


  ngOnInit(): void {
  }

}
